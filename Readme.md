# MotionBlocks
A loose port to Unity3D of the "A Point" program found at [dpmanual](https://dpmanual.bitbucket.io/pages/programs.html).
The main page is [here](https://dpmanual.bitbucket.io/index.html).

This version contains "A Point" and "A Grid Point".
Screenshots [here](https://heartgames.itch.io/point).

## Motivation
Unity3D is a complex game engine which is also being used in non-game related fields. 
It can also be used for Creative Coding and has its own advantages and disadvantages in that respect.

I find it useful to make it more accessible to beginner Creative Coders and that is what this loose port attempts to do.

It is important to note that one of the features of the engine is reducing the amount of code needed to create an application.
Unity3D provides many editor features to this respect.

In the first (and only) example that I've attempted to port, the one called "A Point", I've struggled to find the best way to keep the style of the original program and to highlight the engine's features... 
There are many different ways to achieve the same result in programming, but this is even more so in the case of game engines.

## Technical
This version currently works with Unity3D 2019.4.11f1
I am using the Universal Render Pipeline, which should be included already but can be installed as an [additional package](https://docs.unity3d.com/Packages/com.unity.render-pipelines.universal@7.1/manual/InstallURPIntoAProject.html).
The basic examples should work either way. I have also included the Unity PostProcessing effects in the final example explained below, which is a departure from the formal black and white approach of the dpmanual files.

## Details
In most of the scenes I had to modify the Camera distance and Projection to roughly match the values from the dpmanual example.
Check the settings of the camera in every scene to see how the values depend on the resolution, camera size or distance depending on the type of camera (perspective vs. orthogonal).
See screenshot below.

[Camera Settings in Editor Link to Image](https://gitlab.com/tank-t-bird/motionblocks/-/blob/master/Screenshots/camera.png)

## Scenes

The very first scene uses the [PointSphere](https://gitlab.com/tank-t-bird/motionblocks/-/blob/master/Assets/Scripts/PointSphere.cs) script and tries to do everything in code.

## All other scenes
All other scenes use variations of the script: [PointSphereAsPrefab](https://gitlab.com/tank-t-bird/motionblocks/-/blob/master/Assets/Scripts/PointSphereAsPrefab.cs).
In these scenes, several settings are set in the editor, and not in code. I have added comments where applicable.

In Unity, every script needs to be attached to a game object in order to run. I've used empty game objects and called them "Sketch" (as in Processing) in each scene.

A point has no dimensions, so I use a 2D circle or a 3d sphere instead of scaling the line stroke of the point (which is the Processing way).
The Point object is in the Prefabs folder, there are several versions of it depending on the scene and camera settings.
Some Prefabs will only work with certain scenes as their size is set to fit the camera settings.

The application window size is usually set on export in the Player settings in the editor and in the Player export settings, not in code.
Nevertheless the game window resolution in the editor will change how things look! Keep it at 500 x 500 for the proper size in this project.

Unity by default keeps the Camera background color in the back, so it is not necessary to redraw the object in every frame of the Update function.
This creates some interesting issues in translating the code, as the workaround in Unity would be to create and destroy objects every frame instead, which is very inefficient.
I am using Lists instead to store the objects and then loop through them to make them visible or invisible as needed.

The Start function behaves as the Setup function in Processing. There is an Awake function that happens even before the Start function as well.

When using this script with the SpherePoint Sprite Prefab, there are some recommended changes to the maxSize and amplitude in the comments next to the variables.
