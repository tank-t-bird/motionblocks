﻿using System.Collections.Generic;
using MotionBlocks.Extensions;
using UnityEngine;
using UnityEngine.Serialization;

namespace MotionBlocks
{
    public class PointGrid : MonoBehaviour
    {
        public GameObject spherePoint;
        private GameObject _sphereInstance;
        private float _pointSize; // the prefab in this example is 50 units on all axis
        
        // It is inefficient to Instantiate and Destroy the points every update, so this stores them in a List
        private List<Transform> _points = new List<Transform>(); 
        
        private Transform _point;
        private int _gridStep = 50;
        private float _height;
        private float _width;
        private float _mouseHeight;
        private float _randomValue;
        private int _cutOffValue;
        
        private void Awake()
        {
            _pointSize = spherePoint.transform.localScale.x; // let's use the x size, but the others would work just fine
            _height = Screen.height; 
            _width = Screen.width;
            
            for (float y = _pointSize; y <= _height - _pointSize; y += _gridStep)
            {
                for (float x = _pointSize; x <= _width - _pointSize; x += _gridStep)
                {
                    {
                        _sphereInstance = Instantiate(spherePoint, new Vector3(x, y, 0), Quaternion.identity);
                        _sphereInstance.transform.SetParent(transform, false); // makes objects children of Sketch Object
                        _point = _sphereInstance.transform; // stores Instance of the prefab
                        _point.gameObject.SetActive(false); // hides Instance
                        _points.Add(_point); // store Instance in a list
                    }
                }
            }

        }

        private void Update()
        {
            Random.InitState(175); // Set the random seed, otherwise it changes every update and flickers
            
            _mouseHeight = Input.mousePosition.y;

            for (int i = 0; i < _points.Count; i++)
            {
                _randomValue = Random.Range(0,13);
                _cutOffValue = (int)ExtensionMethods.Map(_mouseHeight, 0, _height, 0, 13);
                if (_randomValue > _cutOffValue)
                {
                    _points[i].gameObject.SetActive(true);
                }
                else
                {
                    _points[i].gameObject.SetActive(false);
                }
            }
        }
    }
}

