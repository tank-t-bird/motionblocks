﻿
/* WHAT IS THIS
 * This code is a translation of a sketch from the dpmanual repository.
 * https://dpmanual.bitbucket.io/pages/programs.html
 * I aim to illustrate how Unity3d can be used for similar purposes,
 * that is, to quickly explore visual concepts and interactive experiments.
 *
 * Unity3D is a game engine that replaces much code with checkboxes and settings in its visual editor.
 * This explains why some bits of code from the Processing sketch are simply not here
 *
 * MINIMAL UNITY CLARIFICATIONS AND DIFFERENCES
 * In Unity, every script needs to be attached to a game object in order to run.
 * The script is equivalent to the Processing sketch.
 * In Unity, a point has no dimension, so I use a 2D circle or a 3d sphere instead.
 * The application window size is usually set on export in the Player settings in the editor, not in the code.
 * Unity by default keeps the background color in the back, so it is not necessary to redraw the object
 * in every frame of the Update function.    
 * The Start function is executed once before the Update loop
 * The Awake function happens before the Start function and is used to set things up
 * 
 *
 * CHANGES TO THIS PROGRAM
 * The sphere previously created in code is now a Prefab called Point
 * The Point object is in the Prefabs folder.
 * Select the Sketch object in the Hierarchy and you will see the Point variable slot.
 * Drag the Prefab into the Slot if it is missing
 *
 * FINAL NOTES
 * When using this script with the SpherePoint Sprite Prefab, there are some recommended changes to the maxSize and amplitude
 * in the comments next to the variables below.
 */

using UnityEngine;
using UnityEngine.Serialization;

namespace MotionBlocks

{
    public class PointSphereAsPrefab : MonoBehaviour
    {

        public GameObject spherePoint;
        private GameObject _sphereInstance;
        private Vector3 _newSize;
        private float _minSize = 0.01f;
        public float maxSize = 12.0f; //make 500 when using the Sprite Prefab, make it 12.0f with the Sphere
        [FormerlySerializedAs("speed")] public float scaleSpeed = 0.024f; //make higher when using the Sprite prefab (2x or more), make it 0.024f with the Sphere

        // todo private float startSize = 1;
        
        private void Awake()
        {
            _newSize = new Vector3(scaleSpeed, scaleSpeed, scaleSpeed);
        }
        private void Start()
        {
            // Make a new instance of the prefab so we don't overwrite the original values
            _sphereInstance = Instantiate(spherePoint, new Vector3(0, 0, 0), Quaternion.identity);
        }

        private void Update()
        {
            _sphereInstance.transform.localScale += _newSize;
            if (_sphereInstance.transform.localScale.y < _minSize || _sphereInstance.transform.localScale.y > maxSize)
                _newSize = -_newSize;
        }
    }
}