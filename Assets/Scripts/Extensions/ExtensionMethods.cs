﻿using UnityEngine;

namespace MotionBlocks.Extensions

{
    public class ExtensionMethods : MonoBehaviour
    {
        /// <summary>
        ///     This method mimics the Map function in Processing sketches
        /// </summary>
        public static float Map(float value, float inputFrom, float inputTo, float outputFrom, float outputTo)
        {
            return outputFrom + (value - inputTo) * (outputFrom - outputTo) / (inputTo - inputFrom);
        }
    }
}