﻿/* WHAT IS THIS
 * This code is a translation of a sketch from the dpmanual repository.
 * https://dpmanual.bitbucket.io/pages/programs.html
 * I aim to illustrate how Unity3d can be used for similar purposes,
 * that is, to quickly explore visual concepts and interactive experiments.
 * 
 * The unity way of scaling a "point" (a point has no dimension):
 * I use a small sphere made to look flat. It is also possible to use a 2D shape
 *
 * No attempt at precision was made.
 *
 * Compare this code to the SpherePointAsPrefab.cs file to see how I would do this with less code,
 * using the Unity editor features.
 */

using MotionBlocks.Extensions;
using UnityEngine;

namespace MotionBlocks

{
    public class PointPureCode : MonoBehaviour
    {
        // Note that these sizes are relative to the "size"of the camera (in Orthographic mode to keep it flat)
        private const float Speed = 0.006f;
        private float _minSize = 0f;
        private float _maxSize = 600;
        private float _newValue;
        private GameObject _pointSphere;
        private Renderer _renderer;

        // todo private float startSize = 1;
        private Shader _shader;

        private void Start()
        {
            // For export, this can be set in the editor in the Build Settings -> Player settings 
            // Keep in mind that the Game resolution in the editor can differ from the output resolution,
            // therefore make sure to change this by creating a custom resolution of 500 x 500 underneath the Game
            // window tab dropdown,
            
            Screen.SetResolution(500, 500, false);

            // This can be set in the editor on the camera component properties
            
            Camera main = Camera.main;
            main.clearFlags = CameraClearFlags.SolidColor;
            main.backgroundColor = Color.black;

            // Creating the sphere by code instead of the more usual Unity way of instantiating a Prefab
            // We could also have used a circle shaped Sprite instead, but the code is a little more involved

            _pointSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            _pointSphere.transform.position = new Vector3(0, 0, 0);

            // Changing the default "3D" material type of the Sphere from Standard to flat and white (Unlit shader)
            
            _renderer = _pointSphere.GetComponent<Renderer>();
            _shader = Shader.Find("Unlit/Color");
            var material = _renderer.material;
            material.shader = _shader;
            material.color = Color.white;
        }
        
        private void Update()
        {
            // There is no need to redraw the point in 3D, in Unity the background is always drawn behind the game objects
            // The Map function in Processing does not exist in Unity. So the method is recreated in the ExtensionMethods class.
            // This is why I've added the directive at the top of the program: using MotionBlocks.Extensions;
            
            _newValue = ExtensionMethods.Map(Mathf.Sin(Time.frameCount * Speed), -1, 1, _minSize, _maxSize);
            _pointSphere.transform.localScale = new Vector3(_newValue, _newValue, _newValue);

         
        }
    }
}