﻿// Create a Sprite at start-up.
// Assign a texture to the sprite when the button is pressed.

using UnityEngine;

public class SpriteCreate : MonoBehaviour
{
    public Texture2D tex;
    private Sprite mySprite;
    private SpriteRenderer sr;

    void Awake()
    {
        sr = gameObject.AddComponent<SpriteRenderer>() as SpriteRenderer;
        sr.color = new Color(1f, 1f, 1f, 1.0f);

        transform.position = new Vector3(0f, 0f, 0.0f);
    }

    void Start()
    {
        mySprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
        sr.sprite = mySprite;
    }
}